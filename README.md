# Platform Cooperatives and Free Software

Slides for a presentation.  <!-- View <a href="http://mlncn.github.io//">index.html</a> in a browser, but view the source for my notes. -->

# Built on stack.js

Stack.js is a presentation library with intuitive, scroll-based navigation.

https://mbostock.github.io/stack/
https://github.com/mbostock/stack
